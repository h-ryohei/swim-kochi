import { createTheme } from '@mui/material/styles';
import { indigo, red } from '@mui/material/colors';

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      main: indigo['800'],
    },
    secondary: {
      main: '#EF4056',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#F2F2F2',
    }
  },
});

export default theme;
