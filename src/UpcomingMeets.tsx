import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';

const meets = [
  {date: '3月5日（金）', detail: '四国SC対抗水泳競技大会', pool: 'くろしおアリーナ'},
  {date: '3月5日（金）', detail: '四国SC対抗水泳競技大会2', pool: 'くろしおアリーナ'},
]

const UpcomingMeets = () => {
  const theme = useTheme();
  return (
    <>
      <Typography variant='h4' sx={{ fontWeight: 'bold', ml: 1, mt: 2 }}>大会予定</Typography>
      <Divider 
        sx={{
          "&.MuiDivider-root": {
            borderBottom: `4px solid ${theme.palette.common.black}`
        }}}
      />
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
        sx={{ mt: 1, ml: 1 }}
      >
        {meets.map((meets) => (
          <React.Fragment key={meets.detail}>
            <Grid item xs={4} sm={2} sx={{ mt: 1 }}>
              <Typography variant='body1' sx={{ fontWeight: 'bold' }} paragraph>{meets.date}</Typography>
            </Grid>
            <Grid item xs={8} sm={10} sx={{ mt: 1 }}>
              <Typography variant='body1' sx={{ fontWeight: 'bold' }} paragraph>
                {meets.detail}（{meets.pool}）
              </Typography>
            </Grid>
          </React.Fragment>
        ))}
      </Grid>
    </>
  )
}

export default UpcomingMeets;