import * as React from 'react';
import { Link } from "react-router-dom";
import { useTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Collapse from '@mui/material/Collapse';
import BusinessIcon from '@mui/icons-material/Business';
import CampaignIcon from '@mui/icons-material/Campaign';
import EventNoteIcon from '@mui/icons-material/EventNote';
import YouTubeIcon from '@mui/icons-material/YouTube';
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
import ChairIcon from '@mui/icons-material/Chair';
// import GavelIcon from '@mui/icons-material/Gavel';
// import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import SchemaIcon from '@mui/icons-material/Schema';
// import FavoriteIcon from '@mui/icons-material/Favorite';
import EmailIcon from '@mui/icons-material/Email';
import PoolIcon from '@mui/icons-material/Pool';

import Logo from './img/swim_t.png';
import SwimIcon from './img/swim_t.png';
import DivingIcon from './img/diving_t.png';
import WPIcon from './img/wp_t.png';
import ASIcon from './img/as_t.png';

const drawerWidth = 240;

const menu = [
  {name: 'お知らせ', icon: <CampaignIcon />, link: 'news_list'},
  {name: '大会情報', icon: <EventNoteIcon />, link: 'meet'},
  {name: 'ライブ中継', icon: <YouTubeIcon />, link: 'live'},
  {name: '高知県記録', icon: <EmojiEventsIcon />, link: 'record'},
  {name: '記録関係', icon: <EmojiEventsIcon />, link: 'record'},
]
const federation = [
  {name: '会長挨拶', icon: <ChairIcon />, link: 'greetings'},
  // {name: '定款・規則・規定', icon: <GavelIcon />, link: 'regulations'},
  // {name: '事業計画・報告', icon: <FormatListNumberedIcon />, link: 'report'},
  {name: '委員会', icon: <SchemaIcon />, link: 'committee'},
  // {name: 'スポンサー', icon: <FavoriteIcon />, link: 'sponser'},
  {name: 'お問合せ', icon: <EmailIcon />, link: 'contact'},
]

const clubs = [
  {name: '競泳', icon: SwimIcon, link: 'club_swim'},
  {name: '飛込', icon: DivingIcon, link: 'club_dibing'},
  {name: '水球', icon: WPIcon, link: 'club_wp'},
  {name: 'AS', icon: ASIcon, link: 'club_as'},
]

interface Props {
  window?: () => Window;
}

export default function Menu(props: Props) {
  const { window } = props;
  const theme = useTheme();
  const [menuOpen, setMenuOpen] = React.useState(false);
  const [listOpen, setListOpen] = React.useState(false);

  React.useEffect(() => {
    let timeoutId = setTimeout(() => {setMenuOpen(true)}, 1000);
    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId)
      }
    }
  }, []);

  const handleClick = () => {
    setListOpen(!listOpen);
  };

  const handleDrawerToggle = () => {
    setMenuOpen(!menuOpen);
  };

  const drawer = (
    <div>
      <Link to='/' style={{ textDecoration: 'none' }} onClick={handleDrawerToggle}>
        <div style={{ textAlign: 'center'}}>
          <img src={Logo} alt='高知県水泳連盟' width='160px' height='160px' />
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item></Grid>
            <Grid item>
              <Typography variant="subtitle2" sx={{ fontWeight: 'bold', textAlign: 'left', color: theme.palette.common.white }}>
                一般社団法人
              </Typography>
              <Typography variant="h6" sx={{ fontWeight: 'bold', pb: 1, color: theme.palette.common.white }}>
                高知県水泳連盟
              </Typography>
            </Grid>
            <Grid item></Grid>
          </Grid>
        </div>
      </Link>
      <List dense>
      <Divider 
        sx={{
          "&.MuiDivider-root": {
            borderBottom: `0.5px solid ${theme.palette.common.white}`,
            marginRight: '4px',
            marginLeft: '4px'
        }}}
      />
        {menu.map((menu) => (
        <React.Fragment  key={menu.name}>
          <Link to={`/${menu.link}`} style={{ textDecoration: 'none' }}>
            <ListItem button onClick={handleDrawerToggle}>
              <ListItemIcon sx={{ color: theme.palette.common.white }}>
                {menu.icon}
              </ListItemIcon>
              <ListItemText primary={menu.name} sx={{ color: theme.palette.common.white }}/>
            </ListItem>
              <Divider 
              sx={{
                "&.MuiDivider-root": {
                  borderBottom: `0.5px solid ${theme.palette.common.white}`,
                  marginRight: '4px',
                  marginLeft: '4px'
              }}}
            />
          </Link>
        </React.Fragment>
        ))}
      <ListItemButton onClick={handleClick}>
        <ListItemIcon sx={{ color: theme.palette.common.white }}>
          <BusinessIcon />
        </ListItemIcon>
        <ListItemText primary="連盟情報" sx={{ color: theme.palette.common.white }} />
        {listOpen ? <ExpandLess sx={{ color: theme.palette.common.white }} /> : <ExpandMore sx={{ color: theme.palette.common.white }} />}
      </ListItemButton>
      <Collapse in={listOpen} timeout="auto" unmountOnExit>
        <List dense>
          {federation.map((menu) => (
            <Link key={menu.name} to={`/${menu.link}`} style={{ textDecoration: 'none' }}>
              <ListItem button sx={{ pl: 4 }} onClick={handleDrawerToggle}>
                <ListItemIcon sx={{ color: theme.palette.common.white }}>
                  {menu.icon}
                </ListItemIcon>
                <ListItemText primary={menu.name} sx={{ color: theme.palette.common.white }} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Collapse>
      <Divider 
        sx={{
          "&.MuiDivider-root": {
            borderBottom: `0.5px solid ${theme.palette.common.white}`,
            marginRight: '4px',
            marginLeft: '4px'
        }}}
      />
        <ListItem button key="直営クラブ">
          <ListItemIcon sx={{ color: theme.palette.common.white }}>
            <PoolIcon />
          </ListItemIcon>
          <ListItemText primary="直営クラブ" sx={{ color: theme.palette.common.white }}/>
        </ListItem>
      <List dense>
        {clubs.map((club) => (
          <Link key={club.name} to={`/${club.link}`} style={{ textDecoration: 'none' }}>
            <ListItem button sx={{ color: theme.palette.common.white, pl: 4 }} onClick={handleDrawerToggle}>
              <ListItemIcon>
                <img src={club.icon} width={24} height={24}/>
              </ListItemIcon>
              <ListItemText primary={club.name} sx={{ color: theme.palette.common.white }}/>
            </ListItem>
          </Link>
        ))}
      </List>
    </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
        <AppBar position="fixed" sx={{ opacity: '80%', boxShadow: 'none'}}>
            <Toolbar>
              <div style={{ flexGrow: 1, display: 'inline-flex', alignItems: 'center' }}>
                <Link to='/' style={{ textDecoration: 'none' }} onClick={handleDrawerToggle}>
                  <img src={Logo} alt='高知県水泳連盟' width='50px' height='50px' style={{ marginRight: 8, marginBottom: '6px'}}/>
                </Link>
                <Typography variant="subtitle2" sx={{ fontWeight: 'bold', pt: "6px"}}>
                  一般社団法人&nbsp;
                </Typography>
                <Typography variant="h6" sx={{ fontWeight: 'bold'}}>
                  高知県水泳連盟
                </Typography>
              </div>
              <IconButton
                size="large"
                edge="end"
                color="inherit"
                aria-label="menu"
                onClick={handleDrawerToggle}
              >
                <MenuIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
      <Box
        component="nav"
        sx={{ width: drawerWidth }}
        aria-label="menu drawer"
      >
        <Drawer
          container={container}
          variant="temporary"
          anchor='right'
          open={menuOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            '& .MuiDrawer-paper': { 
              boxSizing: 'border-box', 
              width: drawerWidth, 
              backgroundColor: 'rgba(0, 0, 0, 0.8)',
            },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
    </Box>
  );
}