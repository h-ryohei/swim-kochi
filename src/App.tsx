import * as React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { useTheme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Menu from './Menu';
import Top from './Top';
import News from './News';
import Meet from './Meet';
import Record from './Record';
import Live from './Live';
import NewsList from './NewsList';
import Greetings from './Greetings';
import Committee from './Committee';
import Contact from './Contact';

import Json from './Json';
import Editor from './Editor';

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      Copyright © 一般社団法人 高知県水泳連盟 {new Date().getFullYear()}.
    </Typography>
  );
}

export default function App() {
  const theme = useTheme();
  return (
    <Container disableGutters maxWidth={false}>
      <Box>
        <Menu />
        <Routes>
          <Route path="/" element={<><Top /><News /></>} />
          <Route path="meet" element={<Meet />} />
          <Route path="record" element={<Record />} />
          <Route path="live" element={<Live />} />
          <Route path="news_list" element={<NewsList />} />
          <Route path="greetings" element={<Greetings />} />
          <Route path="committee" element={<Committee />} />
          <Route path="contact" element={<Contact />} />
          <Route path="json" element={<Json />} />
          <Route path="editor" element={<Editor />} />
        </Routes>
        <Copyright />
      </Box>
    </Container>
  );
}
