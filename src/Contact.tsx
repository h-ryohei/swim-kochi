import * as React from 'react';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl'
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import ListSubheader from '@mui/material/ListSubheader';

const Contact = () => {
  const [to, setTo] = React.useState('');

  const handleChange = (event: SelectChangeEvent) => {
    setTo(event.target.value as string);
  };

  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <Typography variant="h6" gutterBottom component="div">
        お問合せ
      </Typography>
      <br />
      <Divider />
      <br />
      <FormControl fullWidth>
        <InputLabel htmlFor="contact_to">お問合せ先</InputLabel>
        <Select
          labelId="contact_to"
          id="contact_to"
          value={to}
          label="お問合せ先"
          onChange={handleChange}
          required
          autoFocus
        >
          <MenuItem value="理事長">理事長</MenuItem>
          <MenuItem value="事務局">事務局</MenuItem>
          <ListSubheader>競技委員会</ListSubheader>
          <MenuItem value="競泳委員会" sx={{ml: 2}}>競泳委員会</MenuItem>
          <MenuItem value="飛込委員会" sx={{ml: 2}}>飛込委員会</MenuItem>
          <MenuItem value="水球委員会" sx={{ml: 2}}>水球委員会</MenuItem>
          <MenuItem value="AS委員会" sx={{ml: 2}}>AS委員会</MenuItem>
          <MenuItem value="OWS委員会" sx={{ml: 2}}>OWS委員会</MenuItem>
          <MenuItem value="記録委員会">記録委員会</MenuItem>
          <MenuItem value="情報システム委員会">情報システム委員会</MenuItem>
          <MenuItem value="普及委員会">普及委員会</MenuItem>
          <MenuItem value="広報委員会">広報委員会</MenuItem>
          <MenuItem value="総務委員会">総務委員会</MenuItem>
          <MenuItem value="会計委員会">会計委員会</MenuItem>
          <MenuItem value="会計委員会">上記以外</MenuItem>
        </Select>
        <TextField
          id="name"
          label="お名前"
          required
          placeholder="お名前"
          sx={{ mt: 1, mb: 1 }}
        />
        <TextField
          id="title"
          label="件名"
          required
          placeholder="件名"
          sx={{ mt: 1, mb: 1 }}
        />
        <TextField
          id="email"
          label="メールアドレス"
          type="email"
          required
          placeholder="メールアドレス"
          sx={{ mt: 1, mb: 1 }}
        />
        <TextField
          id="message"
          label="お問合せ内容"
          required
          placeholder="お問合せ内容"
          multiline
          rows={10}
          sx={{ mt: 1, mb: 1 }}
        />
        <Button variant='contained' sx={{ p: 2, mt: 1, mb: 1 }} >
          送信
        </Button>
      </FormControl>
    </Container>
    );
};

export default Contact;