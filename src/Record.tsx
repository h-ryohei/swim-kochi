import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import recordData from './data/record.json';
/*
"id": "26",
"category": "高知県記録",
"poolsize": "長水路",
"sex": "女子",
"style": "自由形",
"distance": "400m",
"time": 26263,
"swimmer1": "早本",
"swimmer2": "吏沙",
"swimmer3": null,
"swimmer4": null,
"team": "高知県",
"place": "長良川スイミングプラザ",
"date": "2012-09-17",
"meet": "国民体育大会"
*/

const categoryList = ["高知県記録", "高知県学童記録", "高知県中学記録", "高知県高校記録"];
const poolsizeList = ["長水路", "短水路"];
const sexList = ["男子", "女子"];

function getTime(time: number) {
  if(time > 6000) {
    let minute = Math.floor(time / 6000);
    let second = Math.floor((time - minute * 6000) / 100);
    let msec = Math.floor(time - minute * 6000 - second * 100);
    return `${minute}:${( '00' + second ).slice( -2 )}:${( '00' + msec ).slice( -2 )}`;
  }else{
    let second = Math.floor(time / 100);
    let msec = Math.floor(time - second * 100);
    return `${second}:${( '00' + msec ).slice( -2 )}`;
  }
}

export default function BasicTable() {
  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      {categoryList.map((category) => (
        <React.Fragment key={category}>
          <Typography variant='h6'>{category}</Typography>
          {poolsizeList.map((poolsize) => (
            sexList.map((sex) => (
              <Accordion key={`${category}_${poolsize}_${sex}`}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>{poolsize} {sex}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <TableContainer component={Paper}>
                    <Table sx={{ minWidth: '100%' }} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>泳法</TableCell>
                          <TableCell>距離</TableCell>
                          <TableCell>タイム</TableCell>
                          <TableCell>選手</TableCell>
                          <TableCell>チーム</TableCell>
                          <TableCell>場所</TableCell>
                          <TableCell>樹立日</TableCell>
                          <TableCell>大会</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {recordData.map((row) => (
                          (row.category === category && row.poolsize === poolsize && row.sex === sex) && 
                          (
                            <TableRow
                              key={row.id}
                              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                              <TableCell component="th" scope="row">
                                {row.style}
                              </TableCell>
                              <TableCell align="right">{row.distance}</TableCell>
                              <TableCell align="right">{getTime(row.time)}</TableCell>
                              <TableCell align="left">{row.swimmer1} {row.swimmer2}{(row.style === "フリーリレー" || row.style === "メドレーリレー") && (` ${row.swimmer3} ${row.swimmer4}`)}</TableCell>
                              <TableCell align="left">{row.team}</TableCell>
                              <TableCell align="left">{row.place}</TableCell>
                              <TableCell align="left">{row.date}</TableCell>
                              <TableCell align="left">{row.meet}</TableCell>
                            </TableRow>
                          )
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </AccordionDetails>
              </Accordion>
            ))
          ))}
          <br />
        </React.Fragment>
      ))}
    </Container>
  );
}
