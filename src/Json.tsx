import * as React from 'react';
import Container from '@mui/material/Container';
import meetData from './data/meet.json';

interface Data {
  id: number|null;
  code: number|null;
  fromDate: number|string|null;
  toDate: number|string|null;
  name: string|null;
  deadline: number|string|null;
  guideline: number|string|null;
  place: string|null;
  poolsize: string|null;
  result: number|null;
}
  
function createData(
  id: number|null,
  code: number|null,
  fromDate: number|string|null,
  toDate: number|string|null,
  name: string|null,
  deadline: number|string|null,
  guideline: number|string|null,
  place: string|null,
  poolsize: string|null,
  result: number|null,
): Data {
  return {
    id,
    code,
    fromDate,
    toDate,
    name,
    deadline,
    guideline,
    place,
    poolsize,
    result
  };
}

const rows = meetData.map((data) => {
  return createData(
    data.id,
    data.code,
    data.fromDate,
    data.toDate,
    data.name,
    data.deadline,
    data.guideline,
    data.place,
    data.poolsize,
    data.result
    )
});

const Json = () => {
  const convertDate = (date: string|number|null, open: string|number|null) => {
    let returnValue = null;
    let open_local = (typeof open === 'string') ? open : "00:00";
    if(typeof date === 'string') {
      returnValue = Date.parse(date + " " + open_local);
    }
    return (returnValue);
  }

  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <div>
        [
          {rows.map((row) => {
            return (
              <div key={row.id}>
                {'{'}
                "id": {row.id},
                "code": {row.code ? row.code : "null" },
                "fromDate": {convertDate(row.fromDate, null) ? convertDate(row.fromDate, null) : "null" },
                "toDate": {convertDate(row.toDate, null) ? convertDate(row.toDate, null) : "null" },
                "name": "{row.name}",
                "deadline": {convertDate(row.deadline, null) ? convertDate(row.deadline, null) : "null" },
                "guideline": {row.guideline ? row.guideline : 0},
                "place": "{row.place}",
                "poolsize": "{row.poolsize}",
                "result": 0
                {'},'}
              </div>
            )
          })}
        ]
      </div>
    </Container>
  );
};

export default Json;
