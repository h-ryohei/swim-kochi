import * as React from 'react';
import { Link } from "react-router-dom";
import { useTheme } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

import TopPic1 from './img/top_pic1.jpg';
import TopPic2 from './img/top_pic2.jpg';
import TopPic3 from './img/top_pic3.jpg';
import TopPic4 from './img/top_pic4.jpg';
import TopPic5 from './img/top_pic5.jpg';
import TopPic6 from './img/top_pic6.jpg';
import TopPic7 from './img/top_pic7.jpg';

import newsData from './data/news.json';

const News = () => {
  const theme = useTheme();
  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      alignItems="stretch"
      pl={1}
      pt={'10vh'}
    >
        {newsData.map((news) => (
          news.id < 6 &&
          <React.Fragment key={news.id}>
            <Grid item xs={12} sm={4} pr={1} pb={2}>
            <Link to={`/${news.link}`} style={{ textDecoration: 'none' }}>
              <Paper square elevation={0} sx={{ position: 'relative', height: '200px', borderLeft: `16px solid ${theme.palette.secondary.main}` }}>
                <img
                  src={TopPic2}
                  style={{
                    objectFit: 'cover',
                    width: '100%',
                    height: '100%',
                    objectPosition: 'center center',
                  }}
                />
                <img style={{ position: 'absolute', left: 0, top: 0, width: '100%', height: '100%', background: 'linear-gradient(135deg, rgba(255,255,255,0.2), 50%, rgba(0,0,0,0.4))' }}/>
                <Typography variant="subtitle1" component="div" sx={{ position: 'absolute', right: 0, top: 0, color: 'white', fontWeight: 'bold', pr: 1}}>
                  {news.date}
                </Typography>
                <Typography gutterBottom variant="h6" component="div" sx={{ position: 'absolute', left: 0, bottom: 0, color: 'white', fontWeight: 'bold', pr: 1, pl: 1}}>
                  {news.detail}
                </Typography>
              </Paper>
              </Link>
            </Grid>
          </React.Fragment>
        ))}
    </Grid>
  )
}

export default News;