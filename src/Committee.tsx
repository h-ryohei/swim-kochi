import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import CircleIcon from '@mui/icons-material/Circle';
import RemoveIcon from '@mui/icons-material/Remove';
import ListSubheader from '@mui/material/ListSubheader';
import ListItemText from '@mui/material/ListItemText';

const Committee = () => {
  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <Typography variant="h6" gutterBottom component="div">
        委員会
      </Typography>
      <br />
      <Divider />
      <br />
      <Box>
      <List>
        <ListItem>
          <ListItemIcon>
            <CircleIcon />
          </ListItemIcon>
          <ListItemText primary="理事長" />
        </ListItem>
        <ListItem>
          <List>
            <ListItem>  
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon>    
              <ListItemText primary="事務局" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="競技委員会" secondary="本連盟主催競技会の運営・審判 / 競技役員の講習会の実施 / 審判技術の向上と新規競技役員の養成" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="競技力強化委員会" secondary="競技力強化事業 / 合宿・研修の実施 / コーチ派遣" />
            </ListItem>
            <ListItem>
              <List>
                <ListItem>
                  <ListItemIcon>
                    <RemoveIcon />
                  </ListItemIcon> 
                  <ListItemText primary="競泳委員会" />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <RemoveIcon />
                  </ListItemIcon> 
                  <ListItemText primary="飛込委員会" />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <RemoveIcon />
                  </ListItemIcon> 
                  <ListItemText primary="水球委員会" />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <RemoveIcon />
                  </ListItemIcon> 
                  <ListItemText primary="AS委員会" />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <RemoveIcon />
                  </ListItemIcon> 
                  <ListItemText primary="OWS委員会" />
                </ListItem>
              </List>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="記録委員会" secondary="本連盟主催競技会における記録の管理" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="情報システム委員会" secondary="日本水泳連盟への団体・競技者登録 / 本連盟主催競技会のプログラム作成やホームページ作成" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="普及委員会" secondary="水泳の普及・発展のため、指導者養成事業・指導者派遣事業・泳力検定事業・指導者研修事業" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="広報委員会" secondary="『年鑑水泳こうち』の作成" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="総務委員会" secondary="各種会議の開催 / 各種の渉外業務" />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <CircleIcon />
              </ListItemIcon> 
              <ListItemText primary="会計委員会" secondary="" />
            </ListItem>
          </List>
        </ListItem>
      </List>
      </Box>
      <br />
      <Divider />
      <br />
    </Container>
  );
};

export default Committee;
