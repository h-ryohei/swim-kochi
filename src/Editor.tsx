import * as React from 'react';
import { styled } from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import jaLocale from 'date-fns/locale/ja';

import IconButton from '@mui/material/IconButton';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import Grid from '@mui/material/Grid';

const Input = styled('input')({
  display: 'none',
});

import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel';
import FormControlLabel from '@mui/material/FormControlLabel'
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import ListSubheader from '@mui/material/ListSubheader';

import Switch from '@mui/material/Switch';
import { isSetAccessorDeclaration } from 'typescript';

const Editor = () => {
  const [id, setId] = React.useState<number|null>(null);
  const [code, setCode] = React.useState<number|null>(null);
  const [fromDate, setFromDate] = React.useState<Date | null>(null);
  const [toDate, setToDate] = React.useState<Date | null>(null);
  const [name, setName] = React.useState<string|null>(null);
  const [deadline, setDeadline] = React.useState<Date | null>(null);
  const [guideline, setGuideline] = React.useState<boolean>(false);
  const [place, setPlace] = React.useState<string|null>(null);
  const [poolsize, setPoolsize] = React.useState<"長"|"短"|null>(null);
  const [result, setResult] = React.useState<boolean>(false);

  const [PDF, setPDF] = React.useState<File|undefined>(undefined);
  const [imageFile, setImageFile] = React.useState<File|undefined>(undefined);

  const handleChangePoolsize = (event: SelectChangeEvent) => {
    if(event.target.value === "長" || event.target.value === "短") {
      setPoolsize(event.target.value);
    }else{
      setPoolsize(null);
    }
  };

  const uploadPDF = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files || event.target.files.length === 0) {
      setGuideline(false);
      setPDF(undefined)
      return
    }
    setGuideline(true);
    setPDF(event.target.files[0]);
  }

  const uploadImage = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files || event.target.files.length === 0) {
      setImageFile(undefined)
      return
    }
    setImageFile(event.target.files[0])
    let imgTag = document.getElementById("preview") as HTMLImageElement;
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      const result: string = reader.result as string;
      imgTag.src = result;
    }
  }

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} locale={jaLocale}>
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <Typography variant="h6" gutterBottom component="div">
        大会情報入力
      </Typography>
      <br />
      <Divider />
      <br />
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={6} sx={{ pr: { sm: 1 } }}>
          <FormControl fullWidth>
            <TextField
              id="id"
              label="大会ID"
              autoFocus
              defaultValue={id}
              type="number"
              placeholder="大会ID"
              sx={{ mt: 1, mb: 1 }}
              inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
              onChange={(e) => {setId(parseInt(e.target.value))}}
            />
            <TextField
              id="code"
              label="大会コード"
              defaultValue={code}
              type="number"
              placeholder="大会コード"
              sx={{ mt: 1, mb: 1 }}
              inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
              onChange={(e) => {setCode(parseInt(e.target.value))}}
            />
            <DatePicker
              label="大会初日"
              mask="____年__月__日"
              inputFormat="yyyy年MM月dd日"
              value={fromDate}
              onChange={(newValue) => {setFromDate(newValue)}}
              renderInput={(params) => <TextField {...params} sx={{ mt: 1, mb: 1 }} required/>}
            />
            <DatePicker
              label="大会最終日"
              mask="____年__月__日"
              inputFormat="yyyy年MM月dd日"
              value={toDate}
              onChange={(newValue) => {setToDate(newValue)}}
              renderInput={(params) => <TextField {...params} sx={{ mt: 1, mb: 1 }} />}
            />
            <TextField
              id="name"
              label="大会名"
              required
              placeholder="大会名"
              sx={{ mt: 1, mb: 1 }}
              onChange={(e) => {setName(e.target.value)}}
            />
            <DatePicker
              label="申込締切日"
              mask="____年__月__日"
              inputFormat="yyyy年MM月dd日"
              value={deadline}
              onChange={(newValue) => {setDeadline(newValue)}}
              renderInput={(params) => <TextField {...params} sx={{ mt: 1, mb: 1 }} />}
            />
            <FormLabel component="legend" sx={{ mt: 1, mb: 1 }}>要項</FormLabel>
            <label htmlFor="icon-button-detail">
              <Input accept=".pdf" id="icon-button-detail" type="file" onChange={uploadPDF} />
              <IconButton color="primary" aria-label="要項アップロード" component="span">
                <AttachFileIcon />
              </IconButton>
            </label>
            <TextField
              id="place"
              label="開場名"
              required
              placeholder="開場名"
              sx={{ mt: 1, mb: 1 }}
              onChange={(e) => {setPlace(e.target.value)}}
            />
            </FormControl>
            <FormControl fullWidth sx={{ mt: 1, mb: 1 }}>
              <InputLabel id="demo-simple-select-helper-label">水路</InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                id="poolsize"
                value={(poolsize !== null) ? poolsize : "その他"}
                label="水路"
                required
                onChange={handleChangePoolsize}
              >
                <MenuItem value="長">長水路</MenuItem>
                <MenuItem value="短">短水路</MenuItem>
                <MenuItem value="その他">その他</MenuItem>
              </Select>
            </FormControl>
            <FormControl fullWidth>
              <FormLabel component="legend" sx={{ mt: 1, mb: 1 }}>競技結果</FormLabel>
              <FormControlLabel
                control={
                  <Switch
                    checked={result}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {setResult(event.target.checked)}}
                  />
                }
                label="結果あり"
              />
              <FormLabel component="legend" sx={{ mt: 1, mb: 1 }}>イメージ画像</FormLabel>
              <label htmlFor="icon-button-image">
                <Input accept="image/*" id="icon-button-image" type="file" onChange={uploadImage} />
                <IconButton color="primary" aria-label="イメージ画像" component="span">
                  <AttachFileIcon />
                </IconButton>
              </label>
              <Button variant='contained' sx={{ p: 2, mt: 1, mb: 1 }} >
                送信
              </Button>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6} sx={{ pl: { sm: 1 } }}>
            <TextField
              id="json"
              label="JSON文字列"
              sx={{ mt: 1, mb: 1 }}
              InputProps={{
                readOnly: true,
              }}
              multiline
              fullWidth
              value={`{
  "id": ${id},
  "code": ${code ? code : "null" },
  "fromDate": ${(fromDate !== null) ? Date.parse(fromDate.toDateString()) : "null" },
  "toDate": ${(toDate !== null) ? Date.parse(toDate.toString()) : "null" },
  "name": ${(name !== null) ? '"' + name + '"' : "null"},
  "deadline": ${(deadline !== null) ? Date.parse(deadline.toString()) : "null" },
  "guideline": ${guideline ? guideline : false},
  "place": ${(place !== null) ? '"' + place + '"' : "null"},
  "poolsize": ${(poolsize !== null) ? '"' + poolsize + '"' : "null"},
  "result": ${(result) ? 1 : 0},
  "pdf": ${(PDF !== undefined) ? PDF : "null"}
  "image": ${(imageFile !== undefined) ? imageFile : "null"}
}`}
            />
            {/* <div key={id}>
              {'{'}
              "id": {id},
              "code": {code ? code : "null" },
              "fromDate": {(fromDate !== null) ? Date.parse(fromDate.toDateString()) : "null" },
              "toDate": {(toDate !== null) ? Date.parse(toDate.toString()) : "null" },
              "name": "{name}",
              "deadline": {(deadline !== null) ? Date.parse(deadline.toString()) : "null" },
              "guideline": {guideline ? guideline : 0},
              "place": "{place}",
              "poolsize": "{poolsize}",
              "result": {result ? 1 : 0}
              {'},'}
            </div> */}
            <img id="preview" src="" style={{ width: "100%", objectFit: 'contain'}}></img>
          </Grid>
        </Grid>
      </Container>
    </LocalizationProvider>
  )
}

export default Editor;