import * as React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Avatar from '@mui/material/Avatar';
import Nakanishi from './img/nakanishi.jpeg';

const Greetings = () => {
  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <Grid
        container
        direction="row"
        justifyContent="flex-end"
        alignItems="stretch"
      >
        <Grid item xs={12}>
          <Typography variant="h6" gutterBottom component="div">
            ご挨拶
          </Typography>
        </Grid>
        <Grid item sx={{ textAlign: 'center' }}>
          <Avatar sx={{ width: 120, height: 160, m: 1 }} alt="中西 清二" src={Nakanishi} />
          <Typography variant="subtitle2" component="p">(一社) 高知県水泳連盟</Typography>
          <Typography variant="subtitle2" component="p">会長 中西 清二</Typography>
        </Grid>
      </Grid>
      <br />
      <Divider />
      <br />
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>本連盟の活動目的は、「高知県における水泳及び水泳競技の健全なる普及と発展を図ること」としています。この目的達成のために競技力向上や競技会の開催、指導者や競技役員の育成及び各種講瑠会の開催などを実施しています。</Typography>
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>競技会は常に選手のための大会であり、主役は選手であることをモットーに、また、人間形成の場であることとし、水泳のルールはもとより生活ルール、マナーの厳守等も指導しています。</Typography>
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>水泳の普及、発展のためにはスイミングクラブ協会との連携は欠かせません。共同してコーチ研修や選手の強化育成合宿なども実施しています。競泳については、各SC、学校を主体に選手育成がなされていますが、そこでは出来ない「飛び込み」、「シンクロ」、「水球」については本連盟直営の高知SCを立ち上げ選手育成を図っています。競技者について申し上げますと、高知国体以来毎年全国大会に出場しており、中でも飛び込みの選手は近年全国でも常時上位に入賞しており、着実に成果を上げるに至っています。競泳、水球、シンクロも上位を狙える選手が育ってきています。</Typography>
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>また、当連盟としては、スポーツ大会が地域の発展に貢献するものとして四国大会や全国大会の誘致にも積極的に取り組んでいるところです。心のこもった大会運営をしていくため、競技運営委員においては、これまでの大会運営事項のチェック、競技規則の再確認など細部に亘るチェックをしていただき万全の態勢で臨む心構えをしています。</Typography>
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>それぞれの事業実施にあたっては沢山の競技役員や施設の充実が必要になります。行政の理解あるご協力や水泳に感心のある方々の本連盟へのご参加とご協力をお願い致します。</Typography>
      <Typography variant="body1" component="p" sx={{ textIndent: '1em', textAlign: 'justify' }}>皆様方におかれましては、本年度も当連盟に対し変わらぬご厚情をいただき、格別のご支援。ご協力を賜りますようお願いいたします。</Typography>
      <br />
      <Divider />
      <br />
    </Container>
  );
};

export default Greetings;
