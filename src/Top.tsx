import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

import TopPic1 from './img/top_pic1.jpg';
import TopPic2 from './img/top_pic2.jpg';
import TopPic3 from './img/top_pic3.jpg';
import TopPic4 from './img/top_pic4.jpg';
import TopPic5 from './img/top_pic5.jpg';
import TopPic6 from './img/top_pic6.jpg';
import TopPic7 from './img/top_pic7.jpg';
import TopBG from './img/top_bg.svg';

const TopPic = [
  { subtitle: '第58回', title: '高知県室内選手権', dateFrom: 1607110465663, dateTo: 1607210465663, image: TopPic1 },
  { subtitle: '第3回', title: 'くろしおカップ', dateFrom: 1607210465663, dateTo: 1607210465663, image: TopPic2 },
  { subtitle: '第58回', title: '高知県室内選手権', dateFrom: 1607110465663, dateTo: 1607110465663, image: TopPic3 },
  { subtitle: '第3回', title: 'くろしおカップ', dateFrom: 1607210465663, dateTo: 1607210465663, image: TopPic4 },
  { subtitle: '第58回', title: '高知県室内選手権', dateFrom: 1607110465663, dateTo: 1607110465663, image: TopPic5 },
  { subtitle: '第3回', title: 'くろしおカップ', dateFrom: 1607210465663, dateTo: 1607210465663, image: TopPic6 },
  { subtitle: '第58回', title: '高知県室内選手権', dateFrom: 1607110465663, dateTo: 1607110465663, image: TopPic7 },
]

type MinHeight = {
  minHeight: number,
};

function useAppBarHeight(): number {
  const {
    mixins: { toolbar },
    breakpoints,
  } = useTheme();
  const toolbarDesktopQuery = breakpoints.up('sm');
  const toolbarLandscapeQuery = `${breakpoints.up('xs')} and (orientation: landscape)`;
  const isDesktop = useMediaQuery(toolbarDesktopQuery);
  const isLandscape = useMediaQuery(toolbarLandscapeQuery);
  let currentToolbarMinHeight;
  if (isDesktop) {
    currentToolbarMinHeight = toolbar[toolbarDesktopQuery];
  } else if (isLandscape) {
    currentToolbarMinHeight = toolbar[toolbarLandscapeQuery];
  } else {
    currentToolbarMinHeight = toolbar;
  }
  return (currentToolbarMinHeight as MinHeight).minHeight;
}

const Top = () => {
  const theme = useTheme();
  const [picno, setPicno] = React.useState(0);

  const handleBack = () => {
    return (
      (picno <= 0)
        ? setPicno(TopPic.length - 1)
        : setPicno(picno-1)
    )
  }

  const handleNext = () => {
    return (
      (picno >= TopPic.length - 1)
        ? setPicno(0)
        : setPicno(picno+1)
    )
  }
  const weekday = ['日', '月', '火', '水', '木', '金', '土'];
  const fromYear = (new Date(TopPic[picno].dateFrom)).getFullYear();
  const fromMonth = (new Date(TopPic[picno].dateFrom)).getMonth() + 1;
  const fromDate = (new Date(TopPic[picno].dateFrom)).getDate();
  const fromDay = weekday[(new Date(TopPic[picno].dateFrom)).getDay()];
  const from = `${fromYear}年${fromMonth}月${fromDate}日(${fromDay})`;

  const toYear = (new Date(TopPic[picno].dateTo)).getFullYear();
  const toMonth = (new Date(TopPic[picno].dateTo)).getMonth() + 1;
  const toDate = (new Date(TopPic[picno].dateTo)).getDate();
  const toDay = weekday[(new Date(TopPic[picno].dateTo)).getDay()];
  const to = `${toYear}年${toMonth}月${toDate}日(${toDay})`;

  return (
    <Grid
      container
      direction="column"
      justifyContent="flex-end"
      alignItems="stretch"
    >
      <Grid item>
        <img src={TopPic[picno].image} style={{ position: 'relative', top: 0, left: 0, objectFit: 'cover', width: '100%', height: '75vh', objectPosition: 'center center' }} />
        <img src={TopBG} style={{ position: 'absolute', bottom: `calc(25vh - 1px)`, left: 0, objectFit: 'contain', width: '100%', height: '101%', objectPosition: 'bottom' }} />
        <Paper elevation={0} sx={{ p: 1, position: 'absolute', right: '8px', top: `${useAppBarHeight() + 8 }px`, backgroundColor: 'rgba(255, 255, 255, 0.4)'}}>
          <Typography variant='h6' sx={{ fontWeight: 'bold', color: 'black'}}>{TopPic[picno].subtitle}</Typography>
          <Typography variant='h5' sx={{ fontWeight: 'bold', color: 'black'}}>{TopPic[picno].title}</Typography>
          <Typography variant='body2' sx={{ fontWeight: 'bold', color: 'black', textAlign: 'center'}}>{from}{(from !== to) && ' 〜 ' + to }</Typography>
        </Paper>
        <IconButton aria-label="back" size="large" onClick={handleBack} sx={{ position: 'absolute', right: '8px', top: `calc(75vh - 100px)`}}>
          <ArrowBackIosNewIcon fontSize='large' />
        </IconButton>
        <IconButton aria-label="next" size="large" onClick={handleNext} sx={{ position: 'absolute', right: '8px', top: `calc(75vh - 59px)`}}>
          <ArrowForwardIosIcon fontSize='large' />
        </IconButton>
      </Grid>
    </Grid>
    )
  }
  
  export default Top;