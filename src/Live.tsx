import * as React from 'react';
import Container from '@mui/material/Container';

const Live = () => {
  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      <iframe
        id="youtubeIFrame"
        width="100%"
        height="600px"
        src="https://www.youtube.com/embed/R30X65P_Xwc"
        title="ライブ中継"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      >
      </iframe>
    </Container>
  );
};

export default Live;
