import * as React from 'react';
import Container from '@mui/material/Container';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import newsData from './data/news.json';

const NewsList = () => {
  return (
    <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
      {newsData.map((news) => (
      <Accordion key={news.id}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>{news.date} {news.detail}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            {news.link}
          </Typography>
        </AccordionDetails>
      </Accordion>
      ))}
    </Container>
  );
}

export default NewsList;