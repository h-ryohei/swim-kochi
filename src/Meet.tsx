import * as React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { visuallyHidden } from '@mui/utils';
import IconButton from '@mui/material/IconButton';
import DownloadIcon from '@mui/icons-material/Download';

import Button from '@mui/material/Button';

import meetData from './data/meet.json';

interface Data {
  id: number|null;
  code: number|null;
  fromDate: number|string|null;
  toDate: number|string|null;
  name: string|null;
  deadline: number|string|null;
  guideline: number|string|null;
  place: string|null;
  poolsize: string|null;
  result: number|null;
}
  
function createData(
  id: number|null,
  code: number|null,
  fromDate: number|string|null,
  toDate: number|string|null,
  name: string|null,
  deadline: number|string|null,
  guideline: number|string|null,
  place: string|null,
  poolsize: string|null,
  result: number|null,
): Data {
  return {
    id,
    code,
    fromDate,
    toDate,
    name,
    deadline,
    guideline,
    place,
    poolsize,
    result
  };
}

const rows = meetData.map((data) => {
  return createData(
    data.id,
    data.code,
    data.fromDate,
    data.toDate,
    data.name,
    data.deadline,
    data.guideline,
    data.place,
    data.poolsize,
    data.result
    )
});

const getDateString = (dateNumber: number|string|null) => {
  if(typeof(dateNumber) === "string") {
    return dateNumber;
  }else{
    if(typeof(dateNumber) === "number") {
      let weekday = ['日', '月', '火', '水', '木', '金', '土'];
      let year = (new Date(dateNumber)).getFullYear();
      let month = (new Date(dateNumber)).getMonth() + 1;
      let date = (new Date(dateNumber)).getDate();
      let day = weekday[(new Date(dateNumber)).getDay()];
      let string = `${year}年${month}月${date}日(${day})`;
      return string;
    }
    return null;
  }
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (
  a: { [key in Key]: number | string | null },
  b: { [key in Key]: number | string | null },
) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: 'code',
    numeric: true,
    label: '大会コード',
  },
  {
    id: 'fromDate',
    numeric: true,
    label: '初日',
  },
  {
    id: 'toDate',
    numeric: true,
    label: '最終日',
  },
  {
    id: 'name',
    numeric: false,
    label: '大会名',
  },
  {
    id: 'guideline',
    numeric: false,
    label: '要項',
  },
  {
    id: 'deadline',
    numeric: true,
    label: '申込締切',
  },
  {
      id: 'place',
      numeric: false,
      label: '場所',
  },
  {
      id: 'poolsize',
      numeric: false,
      label: '水路',
  },
  {
    id: 'result',
    numeric: false,
    label: '結果',
},
];

interface EnhancedTableProps {
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, rowCount, onRequestSort } =
    props;
  const createSortHandler =
    (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            // align={headCell.numeric ? 'right' : 'left'}
            align={'left'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

interface EnhancedTableToolbarProps {
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 }
      }}
    >
      <Typography
          sx={{ flex: '1 1 100%' }}
          variant="h6"
          id="tableTitle"
          component="div"
      >
          2022年度 大会予定
      </Typography>
    </Toolbar>
  );
};  

const Meets = () => {
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof Data>('fromDate');
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(50);
  
    const handleRequestSort = (
      event: React.MouseEvent<unknown>,
      property: keyof Data,
    ) => {
      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };
  
    const handleChangePage = (event: unknown, newPage: number) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };
  
    const emptyRows =
      page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const theme = useTheme();

    return (
      <Container sx={{ minHeight: "100vh", minWidth: "100vw", pb: 2, pt: 12}}>
        <Box sx={{ bgcolor: 'rgba(255,255,255,0.8)', borderRadius: 4, maxWidth: "100%"}}>
        <Paper sx={{ maxWidth: "100%", mb: 2, bgcolor: 'transparent', borderRadius: 4}}>
        <EnhancedTableToolbar />
        <TableContainer>
          <Table
            aria-labelledby="tableTitle"
            size='medium'
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={row.id}
                    >
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                      >
                        {row.code}
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                      >
                        {getDateString(row.fromDate)}
                      </TableCell>
                      <TableCell align="left">{getDateString(row.toDate)}</TableCell>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">{(row.guideline === 1) && <a href={"./file/" + row.code + ".pdf"} download><IconButton aria-label="要項" component="div"><DownloadIcon /></IconButton></a>}</TableCell>
                      <TableCell align="left">{getDateString(row.deadline)}</TableCell>
                      <TableCell align="left">{row.place}</TableCell>
                      <TableCell align="left">{row.poolsize}</TableCell>
                      <TableCell align="left">
                        {(row.code !== null) && (
                          <Button 
                            variant="outlined"
                            onClick={() => {
                              window.open("https://result.swim.or.jp/tournament/" + row.code);
                            }}
                            disabled={(row.result !== 1)}
                          >
                            結果
                          </Button>
                        )}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 53 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[50, 100, 200]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
        </Box>
    </Container>
  );
};

export default Meets;
